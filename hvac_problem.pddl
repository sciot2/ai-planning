(define (problem HVAC-Problem) (:domain BIMS)
(:objects 
motionsensor1 - motionsensor 
light1 - light 
door1 - door
rfidsensor1 - rfidsensor
tempsensor1 - tempsensor
fan1 - fan
smokesensor1 - smokesensor
alarm1 - alarm
)

(:init 
    (high-temp tempsensor1)
    (off-fan fan1) 
)

(:goal 
    (not(off-fan fan1)) 
)

;un-comment the following line if metric is needed
;(:metric minimize (???))
)
