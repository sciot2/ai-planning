(define (problem Light-Problem) (:domain BIMS)
(:objects 
motionsensor1 - motionsensor 
light1 - light 
door1 - door
rfidsensor1 - rfidsensor
tempsensor1 - tempsensor
fan1 - fan
smokesensor1 - smokesensor
alarm1 - alarm
)

(:init 
    (detected-motion motionsensor1)
    (off-light light1)    
)

(:goal
    (not(off-light light1))
)

;un-comment the following line if metric is needed
;(:metric minimize (???))
)
