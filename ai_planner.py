
import os
import socketio


old_temp = 0xFF
new_temp = 0xFF
old_motion = "NA"
new_motion = "NA"
old_smoke = "NA"
new_smoke = "NA"
thr_temp = 22
sio = socketio.Client()

@sio.on("config_message")
def message(data):
    global thr_temp
    if(data['topic']=='config_thr_temp'):
        thr_temp = float(data['payload'])
        print("Temperature Thrshold : " + str(thr_temp))

@sio.on("mqtt_message")
def message(data):
    # print(data)
    global old_smoke,old_temp,old_motion
    if(data['topic']=='RFID'):
        tag = str(data['payload']).split('-')
        # print(tag[1])
        monitorAccessControl(tag[1])
            
    elif(data['topic']=='MotionDetect'):
        new_motion = data['payload']
        if(new_motion!=old_motion):
            old_motion = new_motion
            monitorMotionDetector(new_motion) 
            
        
    elif(data['topic']=='Smoke'):
        new_smoke = data['payload']
        if(new_smoke!=old_smoke):
            old_smoke = new_smoke
            monitorSmokeDetector(new_smoke) 

    elif(data['topic']=='Temperature'):
        temp = data['payload']
        if(float(temp) > thr_temp):
            new_temp = "High"
        else:
            new_temp = "Low"      
        if(new_temp!=old_temp):
            old_temp = new_temp
            monitorTemperature(new_temp)    
    
@sio.event
def connect():
    print("AI Planner : Connected!")

@sio.event
def connect_error(data):
    print("AI Planner : Connection failed!")

@sio.event
def disconnect():
    print("AI Planner : Disconnected!")

sio.connect('http://localhost:5000')
sio.emit('connected', "AI Planner Client")

def runMotionDetectorPlanner():
    planner_cmd = '../FF-v2.3/ff -o bims_domain.pddl -f light_problem.pddl > out_motion_detection.txt'
    os.system(planner_cmd)

def runAccessControlPlanner():
    planner_cmd = '../FF-v2.3/ff -o bims_domain.pddl -f access_control_problem.pddl > out_access_control.txt'
    os.system(planner_cmd)

def runSmokeDetectorPlanner():
    planner_cmd = '../FF-v2.3/ff -o bims_domain.pddl -f smoke_problem.pddl > out_smoke_detector.txt'
    os.system(planner_cmd)

def runHvacplanner():
    planner_cmd = '../FF-v2.3/ff -o bims_domain.pddl -f hvac_problem.pddl > out_hvac.txt'
    os.system(planner_cmd)

def monitorAccessControl(rfid):
    output = """
        (define (problem AccessControl-Problem) (:domain BIMS)
                (:objects 
                motionsensor1 - motionsensor 
                light1 - light 
                door1 - door
                rfidsensor1 - rfidsensor
                tempsensor1 - tempsensor
                fan1 - fan
                smokesensor1 - smokesensor
                alarm1 - alarm
               )
        """
    if(rfid=='Grant'):
        output += """
        """ 
        output+= """(:init 
                (matched-rfid rfidsensor1)
                (not(open-door door1))
        )"""
        output += """
        """     
        output+= """(:goal 
                (open-door door1)
        ))"""

    elif(rfid=='Denied'):
        output += """
        """ 
        output+= """(:init 
                (not(matched-rfid rfidsensor1))
                (open-door door1)
        )"""     
        output += """
        """  
        output+= """(:goal 
                (not(open-door door1))
        ))"""
    filename = "../ai-planning/access_control_problem.pddl"
    with open(filename, "w") as f:
        f.write(output)          
    runAccessControlPlanner()
    task = sio.start_background_task(exe_access_control_plan)
    

def monitorMotionDetector(motion):
    output = """
        (define (problem Light-Problem) (:domain BIMS)
                (:objects 
                motionsensor1 - motionsensor 
                light1 - light 
                door1 - door
                rfidsensor1 - rfidsensor
                tempsensor1 - tempsensor
                fan1 - fan
                smokesensor1 - smokesensor
                alarm1 - alarm
               )
        """
    if(motion=='Detected'):
        output += """
        """ 
        output+= """(:init 
                (detected-motion motionsensor1)
                (off-light light1)
        )"""
        output += """
        """     
        output+= """(:goal 
                (not(off-light light1))
        ))"""

    elif(motion=='Not Detected'):
        output += """
        """ 
        output+= """(:init 
                (not(detected-motion motionsensor1))
                (not(off-light light1))
        )"""     
        output += """
        """  
        output+= """(:goal 
                (off-light light1)
        ))"""
    filename = "../ai-planning/light_problem.pddl"
    with open(filename, "w") as f:
        f.write(output)          
    runMotionDetectorPlanner()
    task = sio.start_background_task(exe_motion_detection_plan)

def monitorSmokeDetector(smoke):
    output = """
        (define (problem Smoke-Problem) (:domain BIMS)
                (:objects 
                motionsensor1 - motionsensor 
                light1 - light 
                door1 - door
                rfidsensor1 - rfidsensor
                tempsensor1 - tempsensor
                fan1 - fan
                smokesensor1 - smokesensor
                alarm1 - alarm
               )
        """
    if(smoke=='Detected'):
        output += """
        """ 
        output+= """(:init 
                (high-smoke smokesensor1)
                (off-alarm alarm1)
        )"""
        output += """
        """     
        output+= """(:goal 
                (not(off-alarm alarm1)) 
        ))"""

    elif(smoke=='Not Detected'):
        output += """
        """ 
        output+= """(:init 
                (not(high-smoke smokesensor1))
                (not(off-alarm alarm1))
        )"""
        output += """
        """     
        output+= """(:goal 
                (off-alarm alarm1) 
        ))"""
    filename = "../ai-planning/smoke_problem.pddl"
    with open(filename, "w") as f:
        f.write(output)          
    runSmokeDetectorPlanner()
    task = sio.start_background_task(exe_smoke_plan)


def monitorTemperature(temp):
    output = """
        (define (problem HVAC-Problem) (:domain BIMS)
                (:objects 
                motionsensor1 - motionsensor 
                light1 - light 
                door1 - door
                rfidsensor1 - rfidsensor
                tempsensor1 - tempsensor
                fan1 - fan
                smokesensor1 - smokesensor
                alarm1 - alarm
               )
        """
    if(temp=='High'):
        output += """
        """ 
        output+= """(:init 
                (high-temp tempsensor1)
                (off-fan fan1)
        )"""
        output += """
        """     
        output+= """(:goal 
                (not(off-fan fan1)) 
        ))"""

    elif(temp=='Low'):
        output += """
        """ 
        output+= """(:init 
                (not(high-temp tempsensor1))
                (not(off-fan fan1))
        )"""
        output += """
        """     
        output+= """(:goal 
                (off-fan fan1) 
        ))"""
    filename = "../ai-planning/hvac_problem.pddl"
    with open(filename, "w") as f:
        f.write(output)          
    runHvacplanner()
    task = sio.start_background_task(exe_hvac_plan)


# monitorMotionDetector('Detected')    
# monitorAccessControl('Matched')    
# monitorSmokeDetector('Detected')    
# monitorTemperature('Low')

def exe_access_control_plan():
    file1 = open('out_access_control.txt', 'r')
    count = 0 

    while True:
        count += 1

        # Get next line from file
        line = file1.readline()

        # if line is empty
        # end of file is reached
        if not line:
            break
        if 'step' in line:
            # print("Line{}: {}".format(count, line.strip()))
            text = line.strip().split(":")
            print(text)
            if(text[1]==" DOORCLOSE DOOR1 RFIDSENSOR1"):
                print("Plan : Close Door")
                sio.emit('publish_door_unlock', "Door Close")
            elif(text[1]==" DOOROPEN DOOR1 RFIDSENSOR1"):
                print("Plan : Open Door")
                sio.emit('publish_door_unlock', "Door Open")

    file1.close()

def exe_motion_detection_plan():
    file1 = open('out_motion_detection.txt', 'r')
    count = 0 

    while True:
        count += 1

        # Get next line from file
        line = file1.readline()

        # if line is empty
        # end of file is reached
        if not line:
            break
        if 'step' in line:
            # print("Line{}: {}".format(count, line.strip()))
            text = line.strip().split(":")
            print(text)
            if(text[1]==" SWITCHONLIGHT LIGHT1 MOTIONSENSOR1"):
                print("Plan : Switch On Light")
                sio.emit('publish_light', "Light On")
            elif(text[1]==" SWITCHOFFLIGHT LIGHT1 MOTIONSENSOR1"):
                print("Plan : Switch Off Light")
                sio.emit('publish_light', "Light Off")

    file1.close()    


def exe_hvac_plan():
    file1 = open('out_hvac.txt', 'r')
    count = 0 

    while True:
        count += 1

        # Get next line from file
        line = file1.readline()

        # if line is empty
        # end of file is reached
        if not line:
            break
        if 'step' in line:
            # print("Line{}: {}".format(count, line.strip()))
            text = line.strip().split(":")
            print(text)
            if(text[1]==" SWITCHONFAN FAN1 TEMPSENSOR1"):
                print("Plan : Switch On Fan")
                sio.emit('publish_hvac', "Fan On")
            elif(text[1]==" SWITCHOFFFAN FAN1 TEMPSENSOR1"):
                print("Plan : Switch Off Fan")
                sio.emit('publish_hvac', "Fan Off")

    file1.close()    


def exe_smoke_plan():
    file1 = open('out_smoke_detector.txt', 'r')
    count = 0 

    while True:
        count += 1

        # Get next line from file
        line = file1.readline()

        # if line is empty
        # end of file is reached
        if not line:
            break
        if 'step' in line:
            # print("Line{}: {}".format(count, line.strip()))
            text = line.strip().split(":")
            print(text)
            if(text[1]==" SWITCHONALARM ALARM1 SMOKESENSOR1"):
                print("Plan : Switch On Smoke Alarm")
                sio.emit('publish_smoke_alarm', "Alarm On")
            elif(text[1]==" SWITCHOFFALARM ALARM1 SMOKESENSOR1"):
                print("Plan : Switch Off Smoke Alarm")
                sio.emit('publish_smoke_alarm', "Alarm Off")

    file1.close()    