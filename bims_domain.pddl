(define (domain BIMS)

    (:requirements
        :strips
        :typing
        :negative-preconditions
        
    )

    (:types
        sensor actuator - object
        tempsensor smokesensor rfidsensor motionsensor - sensor 
        fan alarm door light - actuator
    )

    (:predicates
        (light ?l)
        (motionsensor ?o)
        (tempsensor ?t)
        (smokesensor ?m)
        (fan ?f)
        (alarm ?a)
        (door ?d)
        (rfidsensor ?r)

        (off-light ?l - light) 
        (detected-motion ?o - motionsensor)
        (matched-rfid ?r - rfidsensor)
        (open-door ?d - door) 
        (high-temp ?t - tempsensor)
        (off-fan ?f - fan)
        (high-smoke ?m - smokesensor)
        (off-alarm ?a - alarm)
    )

  ;; comment
    (:action switchonlight
        :parameters (?l - light  ?o - motionsensor )
        :precondition (and (detected-motion  ?o)(off-light ?l))
        :effect (not(off-light ?l))
    )
    
    (:action switchofflight
        :parameters (?l - light ?o - motionsensor  )
        :precondition (and (not(detected-motion ?o))(not(off-light ?l)))
        :effect (off-light ?l)
    )   
   
   (:action dooropen
        :parameters (?d - door  ?r - rfidsensor )
        :precondition (and (matched-rfid ?r)(not(open-door ?d)))
        :effect (open-door ?d)
                       
    )

    (:action doorclose
        :parameters (?d - door  ?r - rfidsensor )
        :precondition (and (not(matched-rfid ?r))(open-door ?d))
        :effect (not(open-door ?d))
                       
    )

    (:action switchonfan
        :parameters (?g - fan ?r - tempsensor  )
        :precondition (and (high-temp ?r)(off-fan ?g))
        :effect (not(off-fan ?g))
                       
    )

     (:action switchofffan
        :parameters (?g - fan ?r - tempsensor )
        :precondition (and (not(high-temp ?r))(not(off-fan ?g)))
        :effect (off-fan ?g)
                       
    )

    (:action switchonalarm
        :parameters (?a - alarm  ?m - smokesensor )
        :precondition (and (high-smoke ?m)(off-alarm  ?a))
        :effect (not(off-alarm  ?a))
                       
    )

     
    (:action switchoffalarm
        :parameters (?a - alarm  ?m - smokesensor )
        :precondition (and (not(high-smoke ?m))(not(off-alarm  ?a)))
        :effect (off-alarm  ?a)
                       
    )

    
)
 


