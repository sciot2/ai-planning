(define (problem AccessControl-Problem) (:domain BIMS)
(:objects 
motionsensor1 - motionsensor 
light1 - light 
door1 - door
rfidsensor1 - rfidsensor
tempsensor1 - tempsensor
fan1 - fan
smokesensor1 - smokesensor
alarm1 - alarm
)

(:init 
    (matched-rfid rfidsensor1)
    (not(open-door door1))
)

(:goal 
    (open-door door1)
)

;un-comment the following line if metric is needed
;(:metric minimize (???))
)
